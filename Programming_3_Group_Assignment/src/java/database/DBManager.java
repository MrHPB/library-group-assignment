
package database;

import controller.User;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
import java.util.Vector;
import java.util.Iterator;

/**
 *
 * @author David & Libby
 */
public class DBManager {
    
    public static void insertUser(User U /*String name, String email, String phoneNumber, String password*/) {
        Database db = Database.getInstance(); //Singleton
         String sql = "INSERT INTO Usertable (Name, Email, Phonenumber, Password, admin) VALUES (\"" + U.getUName() + "\", \"" + U.getEmail() + "\", \"" + U.getPhoneNum() +"\", \"" + U.getPassword() + "\" ,\"0\");";
         
        try {
            int update = db.update(sql);
        } catch (SQLException ex) {
            System.out.println("Failed to insert new user");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public static void printUsers(Vector<User> v){
       Iterator<User> it = v.iterator();
       while(it.hasNext()){
           System.out.print(it.next());
       }
    }
    
    public static Vector<User>  loadUsers(){
        String SQLCMD = "SELECT * FROM users";
        Database db = Database.getInstance();
        Vector<User> v = new Vector<User>();
        try{
            ResultSet rs = db.select(SQLCMD);
            ResultSetMetaData rsmd = rs.getMetaData();
           
            while(rs.next()){
               int id = rs.getInt(1);
               String un = rs.getString(2);
               String pn = rs.getString(3);
               String em = rs.getString(4);
               String pw = rs.getString(5);
               User u = new User(un, pn, em, pw);
               u.setID(id);               
               v.add(u);
          }
        }
        catch(SQLException e){
            e.getMessage();
           // return null;
        }
        
        return v;
    }
    
    public static void insertItem (String title, String itemType, String categoryId, String description) {
        Database db = Database.getInstance();
        String sql = "INSERT INTO itemtable (Title, ItemType, CategoryID, Description, loan) VALUES "
                + "(\"" + title + "\", \"" + itemType + "\", \"" + categoryId + "\", \"" + description + "\", \"0\");";
        
        try {
            int update = db.update(sql);
        } catch (SQLException ex) {
            System.out.println("Failed to insert new item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
	public static void insertCD (String itemID, String artist, String releaseDate) {
        Database db = Database.getInstance();
        String sql = "INSERT INTO cdtable (ItemID, Artist, ReleaseDate) VALUES "
                + "(\"" + itemID + "\", \"" + artist + "\", \"" + releaseDate + "\");";
        
        try {
            int update = db.update(sql);
        } catch (SQLException ex) {
            System.out.println("Failed to insert new CD");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public static void insertBook (String itemID, String author, String publisher, String publishDate, String isbn) {
        Database db = Database.getInstance();
        String sql = "INSERT INTO booktable (ItemID, Author, Publisher, PublishDate, ISBN) VALUES "
                + "(\"" + itemID + "\", \""+ author + "\", \"" + publisher + "\", \"" + publishDate + "\", \"" + isbn + "\");";
        
        try {
            int update = db.update(sql);
        } catch (SQLException ex) {
            System.out.println("Failed to insert new item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public static void insertFilm (String itemID, String director, String releaseDate) {
        Database db = Database.getInstance();
        String sql = "INSERT INTO filmtable (ItemID, Director, ReleaseDate) VALUES "
                + "(\"" + itemID + "\", \"" + director + "\", \"" + releaseDate + "\");";
        
        try {
            int update = db.update(sql);
        } catch (SQLException ex) {
            System.out.println("Failed to insert new film");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	
	
    public static void deleteItem (String itemId) {
        Database db = Database.getInstance();
        String deleteComments = "DELETE FROM commenttable WHERE ItemID = \"" + itemId + "\";";
        
        try {
            int d = db.update(deleteComments);
        } catch (SQLException ex) {
            System.out.println("Failed to delete item comments");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String deleteItem = "DELETE FROM itemtable WHERE ItemID = \"" + itemId + "\";";
        
        String deleteCD = "DELETE FROM cdtable WHERE ItemID = \"" + itemId + "\";";
        
        String deleteFilm = "DELETE FROM filmtable WHERE ItemID = \"" + itemId + "\";";
        
        String deleteBook = "DELETE FROM booktable WHERE ItemID = \"" + itemId + "\";";
        
        String deleteLoan = "DELETE FROM loantable WHERE ItemID = \"" + itemId + "\";";
        
        String deleteRequest = "DELETE FROM requesttable WHERE ItemID = \"" + itemId + "\";";
        
        try {
            //delete all subtables
            int d1 = db.update(deleteCD);
            int d2 = db.update(deleteFilm);
            int d3 = db.update(deleteBook);
            int d4 = db.update(deleteLoan);
            int d5 = db.update(deleteRequest);
            //then foreignkeys are deleted, now delete item table entry
            int d = db.update(deleteItem);
        } catch (SQLException ex) {
            System.out.println("Failed to delete item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void updateItem (String itemId, String title, String itemType, String description, String categoryId, String loan) {
             
        
        Database db = Database.getInstance();
        String updateItem = "UPDATE itemtable SET " + 
                "Title = \"" + title
                + "\", ItemType = \"" + itemType
                + "\", CategoryID = \"" + categoryId
                + "\", Description = \"" + description
                + "\", loan = \"" + loan
                + "\" WHERE ItemID = \"" + itemId + "\";";
        
        
        try {
            int update = db.update(updateItem);
        } catch (SQLException ex) {
            System.out.println("Failed to update item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addComment ( String itemId, int userId, String content) {
        Database db = Database.getInstance();
        String sql = "INSERT INTO commenttable (ItemID, UserID, Content) VALUES "
                + "(\"" + itemId + "\", \"" + userId + "\", \"" + content + "\");";
        
        try {
            System.out.println("Adding new comment");
            int update = db.update(sql);
        } catch (SQLException ex) {
            System.out.println("Failed to add new comment");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void loanItem (String itemId, int userId, String returnDate) {
        Database db = Database.getInstance();
        String insertLoan = "INSERT INTO loantable (ItemID, UserID, ReturnDate) VALUES (\"" + itemId + "\", \"" + userId + "\", \"" + "2019-01-01" + "\");";
        try {
            int insert = db.update(insertLoan);
        } catch (SQLException ex) {
            System.out.println("Failed to create loan");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String updateItem = "UPDATE itemtable SET loan = \"1\" WHERE ItemID = \"" + itemId + "\";";
        try {
            int update = db.update(updateItem);
        } catch (SQLException ex) {
            System.out.println("Failed to update item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void requestItem (int userId, String itemId) {
        Database db = Database.getInstance();
        String insertRequest = "INSERT INTO requesttable ( UserID, ItemID) VALUES (\"" + userId + "\", \"" + itemId + "\");";
        try {
            int insert = db.update(insertRequest);
        } catch (SQLException ex) {
            System.out.println("Failed to create request");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void returnItem (String itemId) {
        Database db = Database.getInstance();
        // delete loan, update item to say not loaned
        String deleteLoan = "DELETE FROM loantable WHERE ItemID = \"" + itemId + "\";";
        
        try {   
            int d1 = db.update(deleteLoan);
        } catch (SQLException ex) {
            System.out.println("Failed to delete item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String updateItem = "UPDATE itemtable SET loan = \"0\" WHERE ItemID = \"" + itemId + "\";";
        
        try {
            int update = db.update(updateItem);
        } catch (SQLException ex) {
            System.out.println("Failed to update item");
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
