
package database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David
 */
public class Database {

    private static Database database = null;
    private Connection conn;
    private Statement statement;
    private static final String SERVER_NAME = "jdbc:mysql://localhost:3306/librarydatabase";
    private static final String USER_NAME = "root";
    public static final String PASSWORD = ""; //normally root
    

    private Database() {
        this.conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, e);
        }

        try {
            conn = (Connection) DriverManager.getConnection(SERVER_NAME, USER_NAME, "" /*, PASSWORD */);
        } catch (SQLException e) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, e);
        }
        
        try {
            statement = (Statement) conn.createStatement();
        } catch (SQLException e) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, e);
        }

        System.out.println("Database online");
        
    }

    public static Database getInstance() {
        if (database == null) {
            database = new Database();
        }
        return database;
    }

    public ResultSet select(String sql) throws SQLException {
        ResultSet rs = statement.executeQuery(sql);
        return rs;
    }

    public int update(String sql) throws SQLException {
        int result = statement.executeUpdate(sql);
        return result;
    }

}
