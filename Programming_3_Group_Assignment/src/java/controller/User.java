
package controller;


public class User {
//members
    private int id;
    private String uName, phoneNum, email, password;
        
//setters
    public void setID(int n){
        this.id = n;
    }
    public void setUName(String un){
        this.uName = un;
    }
    
    public void setphoneNum(String pn){
        this.phoneNum = pn;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
   
    public int getID(){return this.id;}
    public String getUName(){return this.uName;}
    public String getPhoneNum(){return this.phoneNum;}
    public String getEmail(){return this.email;}
    public String getPassword(){return this.password;}
    
    
    public User(String un, String email, String phoneNum, String password){
        this.setUName(un);
        this.setEmail(email);
        this.setphoneNum(phoneNum); 
        this.setPassword(password); 
    }
    
    public String toString(){
        String out = "";
        out+=String.format("%d\t%s\t%s\t%s\t%s%n", this.id, this.uName, this.phoneNum, this.email, this.password);
        return out;
    }
        
}
