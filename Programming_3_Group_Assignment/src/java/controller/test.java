
package controller;

import controller.User;
import database.DBManager;

/**
 *
 * @author David
 */
public class test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
       // to start the database, run mySQL with the following command in the bin: 
       // mysql –h localhost –u root librarydatabase 
       // then run the librarydatabase.sql file to create the database 
       // \. \Users\david\Dropbox\Programming_3\librarydatabase.sql
       //*note that the path will be different, librarydatabase.sql lives with the project so point it to there*
        
       DBManager.insertUser(new User("david", "david@hotmail.com", "040000109", "password"));
       DBManager.insertUser(new User("daniel", "daniel@hotmail.com", "040008888", "password"));
      
       
       DBManager.printUsers(DBManager.loadUsers());
       
    }
    
}
