package controller;

import database.DBManager;
import entity.Usertable;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author David
 */
@WebServlet(name = "mainControllerSrv",  loadOnStartup = 1, urlPatterns = {"/mainControllerSrv"})
        
public class mainControllerSrv extends HttpServlet {
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // get the path of the page that requested the get
        String userPath = request.getServletPath();
        System.out.println(userPath);
//        response.sendRedirect(request.getContextPath() + "/index.jsp");
        
        if (userPath.equals("/search")) {
            String title = request.getParameter("title");
            
            HttpSession session = request.getSession();
            session.setAttribute("title", title);
           
          
            response.sendRedirect(request.getContextPath() + "/search.jsp");
        }
        
    }
    

    /**
     * Handles the HTTP <code>POST</code> method. //POST is used in register user
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // get the path of the page that requested the post. 
        String userPath = request.getServletPath();

        // if register page is selected
        if (userPath.equals("/registerAccount")) {

            String userName = request.getParameter("userName");
            String email = request.getParameter("email");
            String phoneNumber = request.getParameter("phoneNumber");
            String password = request.getParameter("password");

            HttpSession session = request.getSession();

            // Error checking, done in a nested if so the error message can be unique to each error. 
            if (!email.contains("@")) {
                session.setAttribute("errorMessage", "Invalid email address");
                response.sendRedirect(request.getContextPath() + "/registerAccount.jsp");
            } else if (userName.isEmpty()) {
                session.setAttribute("errorMessage", "Please enter a user name");
                response.sendRedirect(request.getContextPath() + "/registerAccount.jsp");
            } else if (!phoneNumber.matches("[0-9]+")) {
                session.setAttribute("errorMessage", "Phone number must contain only numbers");
                response.sendRedirect(request.getContextPath() + "/registerAccount.jsp");
            } else if (phoneNumber.length() < 8) {
                session.setAttribute("errorMessage", "Phone number must be at least 8 numbers");
                response.sendRedirect(request.getContextPath() + "/registerAccount.jsp");
            } else {
                if (session.getAttribute("errorMessage") != null) {
                    session.removeAttribute("errorMessage");
                }
                User newUser = new User(userName, email, phoneNumber, password);
                DBManager.insertUser(newUser);
                response.sendRedirect(request.getContextPath() + "/index.jsp");
            }

        }
        if (userPath.equals("/addItem")) {

            String title = request.getParameter("title");
            String itemType = request.getParameter("itemType");
            String categoryID = request.getParameter("categoryID");
            String description = request.getParameter("description");
			
            HttpSession session = request.getSession();
            System.out.println("trying to insertItem ");
            //DBManager.insertItem(title, itemType, categoryID, description);
   
			if ("CD".equals(request.getParameter("itemType"))) 
			{
				
                            String ItemID = request.getParameter("itemID");
                            String Artist = request.getParameter("artist");
				String ReleaseDate = request.getParameter("releaseDate");
				System.out.println("trying to insertCD ");
                                DBManager.insertItem(title, itemType, categoryID, description);
                                DBManager.insertCD(ItemID, Artist,ReleaseDate);
				
			}
			if ("Book".equals(request.getParameter("itemType"))) 
			{
				String ItemID = request.getParameter("itemID");
                                String Author = request.getParameter("author");
				String Publisher = request.getParameter("publisher");
				String PublishDate = request.getParameter("publishDate");
				String ISBN = request.getParameter("isbn");
				System.out.println("trying to insertBook ");
                                //HttpSession session = request.getSession();
                                DBManager.insertItem(title, itemType, categoryID, description);
				DBManager.insertBook(ItemID, Author, Publisher, PublishDate, ISBN);
				
			}
			if ("Film".equals(request.getParameter("itemType"))) 
			{
				String ItemID = request.getParameter("itemID");
                                String Director = request.getParameter("director");
				String ReleaseDate = request.getParameter("releaseDate");
				System.out.println("trying to insertFilm ");
                                //HttpSession session = request.getSession();
                                DBManager.insertItem(title, itemType, categoryID, description);
				DBManager.insertFilm(ItemID, Director, ReleaseDate);
			}	
            //HttpSession session = request.getSession();

            // Error checking, needs to be done in a nested if so the error message can be unique to each error. 
            
            
               response.sendRedirect(request.getContextPath() + "/addItem.jsp"); 
            }
        
        if (userPath.equals("/search")) {
            String itemId = request.getParameter("itemId");
            HttpSession session = request.getSession();

            session.setAttribute("itemId", itemId);
            response.sendRedirect(request.getContextPath() + "/itemPage.jsp");
        }
        
        
         if (userPath.equals("/itemPage")) 
         {
            System.out.println(request.getParameter("addLoan"));
            if (request.getParameter("addComment") != null) {
                String itemId = request.getParameter("itemId");
                Usertable user = (Usertable) request.getSession().getAttribute("user");
                String comment = request.getParameter("comment");

                System.out.println("trying to add comment"+itemId + user.getUserID()+ comment);
                DBManager.addComment(itemId, user.getUserID(), comment);
            }
            
            if (request.getParameter("addLoan") != null) {
                String itemId = request.getParameter("itemId");
                Usertable user = (Usertable) request.getSession().getAttribute("user");
                String loaned = request.getParameter("loaned");
                String returnDate = request.getParameter("returnDate");
                
                System.out.println(loaned);
                
                if (loaned.equals("false")) 
                {
                    System.out.println("adding loan");
                    DBManager.loanItem(itemId, user.getUserID(), returnDate); 
                }
                else
                {
                    System.out.println("already loaned, adding request");
                    DBManager.requestItem(user.getUserID(), itemId); 
                }    
            }
            
            response.sendRedirect(request.getContextPath() + "/items.jsp");
        
    }
           if (userPath.equals("/delete")) {

            String itemId = request.getParameter("itemId");
            System.out.println("attempting to delete item:" +itemId);
            DBManager.deleteItem(itemId);
            response.sendRedirect(request.getContextPath() + "/search.jsp");
        }
           
           
           if (userPath.equals("/return")) {

            String itemId = request.getParameter("itemId");
            System.out.println("attempting to return item:" +itemId);
            DBManager.returnItem(itemId);
            response.sendRedirect(request.getContextPath() + "/accountPage.jsp");
        }
           

        }
    
    

    
    
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
