package controller;

import entity.Usertable;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.UsertableFacade;

/**
 *
 * @author David
 */
@WebServlet(name = "RegisterSrv", urlPatterns = {"/RegisterSrv"})
public class RegisterSrv extends HttpServlet {
    @EJB
    private UsertableFacade usertableFacade;
    @PersistenceUnit
    EntityManagerFactory emf;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String address;
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
            
        out.println(request.getParameter("Email"));
        out.println("Is empty ? " + emf.createEntityManager().createNamedQuery("Usertable.findByEmail").setParameter("email", request.getParameter("Email")).getResultList().get(0));
            
        if (!emf.createEntityManager().createNamedQuery("Usertable.findByEmail").setParameter("email", request.getParameter("Email")).getResultList().isEmpty()){
                    //User email already exists
                    out.println("User already exists");
                }
        else{
            out.println("Registering new user");
            Usertable user = new Usertable();
            user.setEmail(request.getParameter("Email"));
            user.setName(request.getParameter("Name"));
            user.setPassword(request.getParameter("Password"));
            user.setPhonenumber(request.getParameter("PhoneNumber"));
            out.println(usertableFacade.count());

            usertableFacade.create(user);
            
        }
        
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
