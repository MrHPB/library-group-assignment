package controller;

import entity.Usertable;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.console;
import java.util.List;
import javax.ejb.EJB;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.UsertableFacade;

/**
 *
 * @author David
 */
@WebServlet(name = "LoginSrv", urlPatterns = {"/LoginSrv"})
public class LoginSrv extends HttpServlet {
    @EJB
    private UsertableFacade usertableFacade;
    @PersistenceUnit
    EntityManagerFactory emf;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String address;
            Usertable user = null;
            
            response.setContentType("text/html;charset=UTF-8");
                
                System.out.println("No account = " + emf.createEntityManager().createNamedQuery("Usertable.findByEmail").setParameter("email", request.getParameter("Email")).getResultList().isEmpty());
                
                if (emf.createEntityManager().createNamedQuery("Usertable.findByEmail").setParameter("email", request.getParameter("Email")).getResultList().isEmpty()){
                    request.setAttribute("errorMessage", "Account does not exist");
                    address = "login.jsp";
                }
                else{
                    user = (Usertable) emf.createEntityManager().createNamedQuery("Usertable.findByEmail").setParameter("email", request.getParameter("Email")).getResultList().get(0);
                
                    if (user.getPassword().equals(request.getParameter("Password"))){
                        request.getSession().setAttribute("user", user);
                        address = "index.jsp";
                    }
                    else{
                        request.setAttribute("errorMessage", "Incorrect Password");
                        address = "login.jsp";
                    }
                }

                RequestDispatcher dispatcher = request.getRequestDispatcher(address);
                dispatcher.forward(request, response);
                
            
    }


    /**
     * Handles the HTTP <code>GET</code> method.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
