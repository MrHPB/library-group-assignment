/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniel
 */
//JAVA BEAN using persistance API see: https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm#BNBPZ
@Entity
@Table(name = "requesttable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requesttable.findByUserID", query = "SELECT u FROM Requesttable u WHERE u.userID = :userID"),
    @NamedQuery(name = "Requesttable.findByItemID", query = "SELECT u FROM Requesttable u WHERE u.itemID = :itemID")
})

//This java bean needs to be serializable

public class Requesttable implements Serializable{
    /*@JoinTable(name = "bookmarktable", joinColumns = {
        @JoinColumn(name = "UserID", referencedColumnName = "UserID")})
    })*/
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RqstID")
    private Integer rqstID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "UserID")
    private Integer userID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ItemID")
    private Integer itemID;
    
    public Requesttable(){
    }
    
    public Requesttable(int RqstID) {
        this.rqstID = RqstID;
    }
    
    public Requesttable(int RqstID, int UserID, int ItemID){
        this.rqstID = RqstID;
        this.userID = UserID;
        this.itemID = ItemID;
    }
    
    // Gets & Sets
    public Integer getRqstID(){
        return rqstID;
    }
    
    public Integer getUserID(){
        return userID;
    }
    
    public void setUserID(Integer userID){
        this.userID = userID;
    }
    
    public Integer getItemID(){
        return itemID;
    }
    
    public void setItemID(Integer itemID){
        this.itemID = itemID;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rqstID != null ? rqstID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Requesttable)) {
            return false;
        }
        Requesttable other = (Requesttable) object;
        if ((this.rqstID == null && other.rqstID != null) || (this.rqstID != null && !this.rqstID.equals(other.rqstID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Requesttable[ rqstID=" + rqstID + " ]";
    }   
    
}
