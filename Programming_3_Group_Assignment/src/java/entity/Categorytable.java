/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @categoryType Daniel
 */
//JAVA BEAN using persistance API see: https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm#BNBPZ
@Entity
@Table(name = "categorytable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categorytable.findByCategoryType", query = "SELECT u FROM Categorytable u WHERE u.categoryType = :categoryType"),
    @NamedQuery(name = "Categorytable.findByCategoryDesc", query = "SELECT u FROM Categorytable u WHERE u.categoryDesc = :categoryDesc"),
    @NamedQuery(name = "Categorytable.findByCategoryID", query = "SELECT u FROM Categorytable u WHERE u.categoryID = :categoryID")
})

//This java bean needs to be serializable

public class Categorytable implements Serializable{
    /*@JoinTable(name = "bookmarktable", joinColumns = {
        @JoinColumn(name = "UserID", referencedColumnName = "UserID")})
    })*/
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CategoryID")
    private Integer categoryID;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CategoryType")
    private String categoryType;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CategoryDesc")
    private String categoryDesc;
     
    public Categorytable(){
    }
    
    public Categorytable(int CategoryID) {
        this.categoryID = CategoryID;
    }
    
    public Categorytable(int CategoryID, String CategoryType, String CategoryDesc){
        this.categoryID = CategoryID;
        this.categoryType = CategoryType;
        this.categoryDesc = CategoryDesc;
    }
    
    // Gets & Sets

    public Integer getCategoryID() {
        return categoryID;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryID != null ? categoryID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Categorytable)) {
            return false;
        }
        Categorytable other = (Categorytable) object;
        if ((this.categoryID == null && other.categoryID != null) || (this.categoryID != null && !this.categoryID.equals(other.categoryID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Categorytable[ rqstID=" + categoryID + " ]";
    }   
    
}
