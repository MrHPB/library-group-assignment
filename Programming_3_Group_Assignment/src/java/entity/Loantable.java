/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import java.io.Serializable;
import java.util.Collection;
import java.sql.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniel
 */
//JAVA BEAN using persistance API see: https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm#BNBPZ
@Entity
@Table(name = "loantable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Loantable.findByLoanID", query = "SELECT u FROM Loantable u WHERE u.loanID = :loanID"),
    @NamedQuery(name = "Loantable.findByReturnDate", query = "SELECT u FROM Loantable u WHERE u.returnDate = :returnDate"),
    @NamedQuery(name = "Loantable.findByUserID", query = "SELECT u FROM Loantable u WHERE u.userID = :userID"),
    @NamedQuery(name = "Loantable.findByItemID", query = "SELECT u FROM Loantable u WHERE u.itemID = :itemID")
})

//This java bean needs to be serializable

public class Loantable implements Serializable{
    /*@JoinTable(name = "bookmarktable", joinColumns = {
        @JoinColumn(name = "UserID", referencedColumnName = "UserID")})
    })*/
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LoanID")
    private Integer loanID;
    
    @JoinColumn(name = "ItemID", referencedColumnName = "ItemID")
    @ManyToOne(optional = false)
    private Itemtable itemID;
    @JoinColumn(name = "UserID", referencedColumnName = "UserID")
    @ManyToOne(optional = false)
    private Usertable userID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ReturnDate")
    private Date returnDate;
    
    public Loantable(){
    }
    
    public Loantable(int LoanID) {
        this.loanID = LoanID;
    }
    
    public Loantable(int LoanID, int ItemID, int UserID, Date ReturnDate){
        this.loanID = LoanID;
        this.returnDate = ReturnDate;
    }
    
    // Gets & Sets
    public Integer getLoanID(){
        return loanID;
    }
    
   public Usertable getUserID() {
        return userID;
    }

    public void setUserID(Usertable userID) {
        this.userID = userID;
    }
    
   public Itemtable getItemID() {
        return itemID;
    }

    public void setItemID(Itemtable itemID) {
        this.itemID = itemID;
    }
    
    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loanID != null ? loanID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Loantable)) {
            return false;
        }
        Loantable other = (Loantable) object;
        if ((this.loanID == null && other.loanID != null) || (this.loanID != null && !this.loanID.equals(other.loanID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Loantable[ rqstID=" + loanID + " ]";
    }   
    
}
