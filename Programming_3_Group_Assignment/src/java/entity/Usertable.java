
package entity;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David
 */
//JAVA BEAN using persistance API see: https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm#BNBPZ
@Entity
@Table(name = "usertable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usertable.findAll", query = "SELECT u FROM Usertable u"),
    @NamedQuery(name = "Usertable.findByUserID", query = "SELECT u FROM Usertable u WHERE u.userID = :userID"),
    @NamedQuery(name = "Usertable.findByName", query = "SELECT u FROM Usertable u WHERE u.name = :name"),
    @NamedQuery(name = "Usertable.findByEmail", query = "SELECT u FROM Usertable u WHERE u.email = :email"),
    @NamedQuery(name = "Usertable.findByPhonenumber", query = "SELECT u FROM Usertable u WHERE u.phonenumber = :phonenumber"),
    @NamedQuery(name = "Usertable.findByPassword", query = "SELECT u FROM Usertable u WHERE u.password = :password"),
    @NamedQuery(name = "Usertable.findByadmin", query = "SELECT u FROM Usertable u WHERE u.admin = :admin")})

//This java bean needs to be serisalizable 

public class Usertable implements Serializable {
    
    //Join changed from bookmark table to requesttable 23/10/18
    
    @JoinTable(name = "requesttable", joinColumns = {
        @JoinColumn(name = "UserID", referencedColumnName = "UserID")}, inverseJoinColumns = {
        @JoinColumn(name = "ItemID", referencedColumnName = "ItemID")})
    @ManyToMany
    
  
    /* this references the item , comment posting and loaning tables */
    private Collection<Itemtable> itemtableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    
    private Collection<Commenttable> commenttableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
   
    private Collection<Loantable> loantableCollection;
    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "UserID")
    private Integer userID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "Phonenumber")
    private String phonenumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "admin")
    private Integer admin;

    //java bean no arg constructor
    public Usertable() {
    }

    public Usertable(Integer userID) {
        this.userID = userID;
    }

    public Usertable(Integer userID, String name, String email, String phonenumber, String password, Integer admin) {
        this.userID = userID;
        this.name = name;
        this.email = email;
        this.phonenumber = phonenumber;
        this.password = password;
        this.admin = admin;
    }

    //java bean get and set methods
    
    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Integer getAdmin() {
        return admin;
    }

    public void setAdmin(Integer admin) {
        this.admin = admin;
    }
    
    public boolean isAdmin(){
        if (admin > 0){
            return true;
        }
        else{
            return false;
            }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userID != null ? userID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Usertable)) {
            return false;
        }
        Usertable other = (Usertable) object;
        if ((this.userID == null && other.userID != null) || (this.userID != null && !this.userID.equals(other.userID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Usertable[ userID=" + userID + " ]";
    }
    
   
    
    @XmlTransient
    //get item collection for user 
    public Collection<Itemtable> getItemtableCollection() {
        return itemtableCollection;
    }
    
    
     //set the item collection for the user 
    public void setItemtableCollection(Collection<Itemtable> itemtableCollection) {
        this.itemtableCollection = itemtableCollection;
    }
    
    
     //get the table of user posted comments 
    @XmlTransient
    public Collection<Commenttable> getCommentCollection() {
        return commenttableCollection;
    }
    
    
     //set table of user posted comments 
    public void setCommenttableCollection(Collection<Commenttable> commenttableCollection) {
        this.commenttableCollection = commenttableCollection;
    }
    
    
    //get user loan table
    @XmlTransient
    public Collection<Loantable> getLoantableCollection() {
        return loantableCollection;
    }
    
    
    //set user loan table 
    public void setLoantableCollection(Collection<Loantable> loantableCollection) {
        this.loantableCollection = loantableCollection;
    }
    
}
