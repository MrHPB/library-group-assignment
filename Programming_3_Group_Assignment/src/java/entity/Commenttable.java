package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David
 */
@Entity
@Table(name = "commenttable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commenttable.findAll", query = "SELECT c FROM Commenttable c"),
    @NamedQuery(name = "Commenttable.findByCmntID", query = "SELECT c FROM Commenttable c WHERE c.cmntID = :cmntID"),
    @NamedQuery(name = "Commenttable.findByContent", query = "SELECT c FROM Commenttable c WHERE c.content = :content")})
public class Commenttable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CmntID")
    private Integer cmntID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Content")
    private String content;
   
    @JoinColumn(name = "ItemID", referencedColumnName = "ItemID")
    @ManyToOne(optional = false)
    private Itemtable itemID;
   
    @JoinColumn(name = "UserID", referencedColumnName = "UserID")
    @ManyToOne(optional = false)
    private Usertable userID;

    public Commenttable() {
    }

    public Commenttable(Integer postID) {
        this.cmntID = postID;
    }

    public Commenttable(Integer postID, String post) {
        this.cmntID = postID;
        this.content = post;
    }

    public Integer getCmntID() {
        return cmntID;
    }

    public void setCmntID(Integer CmntID) {
        this.cmntID = CmntID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Itemtable getItemID() {
        return itemID;
    }

    public void setItemID(Itemtable itemID) {
        this.itemID = itemID;
    }

    public Usertable getUserID() {
        return userID;
    }

    public void setUserID(Usertable userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cmntID != null ? cmntID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Commenttable)) {
            return false;
        }
        Commenttable other = (Commenttable) object;
        if ((this.cmntID == null && other.cmntID != null) || (this.cmntID != null && !this.cmntID.equals(other.cmntID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Commenttable[ cmntID=" + cmntID + " ]";
    }
    
}
