/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniel
 */
//JAVA BEAN using persistance API see: https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm#BNBPZ
@Entity
@Table(name = "cdtable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CDtable.findByArtist", query = "SELECT u FROM CDtable u WHERE u.artist = :artist"),
    @NamedQuery(name = "CDtable.findByReleaseDate", query = "SELECT u FROM CDtable u WHERE u.releaseDate = :releaseDate"),
    @NamedQuery(name = "CDtable.findByItemID", query = "SELECT u FROM CDtable u WHERE u.itemID = :itemID")
})

//This java bean needs to be serializable

public class CDtable implements Serializable{
    /*@JoinTable(name = "bookmarktable", joinColumns = {
        @JoinColumn(name = "UserID", referencedColumnName = "UserID")})
    })*/
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ItemID")
    private Integer itemID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ReleaseDate")
    private Date releaseDate;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Artist")
    private String artist;
    
    public CDtable(){
    }
    
    public CDtable(int ItemID) {
        this.itemID = ItemID;
    }
    
    public CDtable(int ItemID, String Artist, Date ReleaseDate){
        this.itemID = ItemID;
        this.releaseDate = ReleaseDate;
        this.artist = Artist;
    }
    
    // Gets & Sets

    public Integer getItemID() {
        return itemID;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public String getArtist() {
        return artist;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
    
    

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemID != null ? itemID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CDtable)) {
            return false;
        }
        CDtable other = (CDtable) object;
        if ((this.itemID == null && other.itemID != null) || (this.itemID != null && !this.itemID.equals(other.itemID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CDtable[ rqstID=" + itemID + " ]";
    }   
    
}
