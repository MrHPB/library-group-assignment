package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David
 */
@Entity
@Table(name = "itemtable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Itemtable.findAll", query = "SELECT i FROM Itemtable i"),
    @NamedQuery(name = "Itemtable.findByItemID", query = "SELECT i FROM Itemtable i WHERE i.itemID = :itemID"),
    @NamedQuery(name = "Itemtable.findByTitle", query = "SELECT i FROM Itemtable i WHERE i.title = :title"),
    @NamedQuery(name = "Itemtable.findByItemType", query = "SELECT i FROM Itemtable i WHERE i.itemType = :itemType"),
    @NamedQuery(name = "Itemtable.findByDescription", query = "SELECT i FROM Itemtable i WHERE i.description = :description"),
    @NamedQuery(name = "Itemtable.findByCategoryID", query = "SELECT i FROM Itemtable i WHERE i.categoryID = :categoryID"),
    @NamedQuery(name = "Itemtable.findByLoan", query = "SELECT i FROM Itemtable i WHERE i.loan = :loan")})

public class Itemtable implements Serializable {
    private static final long serialVersionUID = 1L;
    public static String findByCategoryID;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ItemID")
    private Integer itemID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Title")
    private String title;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ItemType")
    private int itemType;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Description")
    private String description;
   
    @Basic(optional = false)
    @NotNull
    @Column(name = "CategoryID")
    private int categoryID;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "loan")
    private boolean loan;
    
    @ManyToMany(mappedBy = "itemtableCollection")
    private Collection<Usertable> usertableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemID")
    private Collection<Commenttable> commenttableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemID")
    private Collection<Loantable> loantableCollection;

    public Itemtable() {
    }

    public Itemtable(Integer itemID) {
        this.itemID = itemID;
    }

    public Itemtable(Integer itemID, String title, int itemType, Integer categoryId, String description, boolean loan) {
        this.itemID = itemID;
        this.itemType = itemType;
        this.title = title;
        this.description = description;
        this.categoryID = categoryID;
        this.loan = loan;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getItemType() {
        return itemType;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public boolean getLoan() {
        return loan;
    }

    public void setLoan(boolean loan) {
        this.loan = loan;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection() {
        return usertableCollection;
    }

    public void setUsertableCollection(Collection<Usertable> usertableCollection) {
        this.usertableCollection = usertableCollection;
    }

    @XmlTransient
    public Collection<Commenttable> getCommenttableCollection() {
        return commenttableCollection;
    }

    public void setCommenttableCollection(Collection<Commenttable> commenttableCollection) {
        this.commenttableCollection = commenttableCollection;
    }

    @XmlTransient
    public Collection<Loantable> getLoantableCollection() {
        return loantableCollection;
    }

    public void setLoantableCollection(Collection<Loantable> loantableCollection) {
        this.loantableCollection = loantableCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemID != null ? itemID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Itemtable)) {
            return false;
        }
        Itemtable other = (Itemtable) object;
        if ((this.itemID == null && other.itemID != null) || (this.itemID != null && !this.itemID.equals(other.itemID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Itemtable[ itemID=" + itemID + " ]";
    }
    
}
