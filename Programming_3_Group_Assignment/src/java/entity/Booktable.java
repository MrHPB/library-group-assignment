/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniel
 */
//JAVA BEAN using persistance API see: https://docs.oracle.com/javaee/7/tutorial/persistence-intro.htm#BNBPZ
@Entity
@Table(name = "booktable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Booktable.findByAuthor", query = "SELECT u FROM Booktable u WHERE u.author = :author"),
    @NamedQuery(name = "Booktable.findByPublisher", query = "SELECT u FROM Booktable u WHERE u.publisher = :publisher"),
    @NamedQuery(name = "Booktable.findByISBN", query = "SELECT u FROM Booktable u WHERE u.isbn = :isbn"),
    @NamedQuery(name = "Booktable.findByPublishDate", query = "SELECT u FROM Booktable u WHERE u.publishDate = :publishDate"),
    @NamedQuery(name = "Booktable.findByItemID", query = "SELECT u FROM Booktable u WHERE u.itemID = :itemID")
})

//This java bean needs to be serializable

public class Booktable implements Serializable{
    /*@JoinTable(name = "bookmarktable", joinColumns = {
        @JoinColumn(name = "UserID", referencedColumnName = "UserID")})
    })*/
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ItemID")
    private Integer itemID;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PublishDate")
    private Date publishDate;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Author")
    private String author;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Publisher")
    private String publisher;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ISBN")
    private String isbn;
    
    public Booktable(){
    }
    
    public Booktable(int ItemID) {
        this.itemID = ItemID;
    }
    
    public Booktable(int ItemID, String Author, String Publisher, Date PublishDate, String ISBN){
        this.itemID = ItemID;
        this.author = Author;
        this.publisher = Publisher;
        this.publishDate = PublishDate;
        this.isbn = ISBN;
    }
    
    // Gets & Sets

    public Integer getItemID() {
        return itemID;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    
    

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemID != null ? itemID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Booktable)) {
            return false;
        }
        Booktable other = (Booktable) object;
        if ((this.itemID == null && other.itemID != null) || (this.itemID != null && !this.itemID.equals(other.itemID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CDtable[ rqstID=" + itemID + " ]";
    }   
    
}
