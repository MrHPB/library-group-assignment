package session;

import entity.Requesttable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daniel
 */
@Stateless
public class RequesttableFacade extends AbstractTableFacade<Requesttable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RequesttableFacade() {
        super(Requesttable.class);
    }
    
}
