package session;

import entity.Commenttable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author David
 */
@Stateless
public class CommenttableFacade extends AbstractTableFacade<Commenttable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommenttableFacade() {
        super(Commenttable.class);
    }
    
}
