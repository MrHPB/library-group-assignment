package session;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author David
 */
//Abstract Class used for all the CRUD operations
//see http://geomobile.como.polimi.it/policrowd/javadoc/edu/polimi/policrowd/service/AbstractFacade.html
public abstract class AbstractTableFacade<Table> { 
    private Class<Table> theEntityClass;

    public AbstractTableFacade(Class<Table> entityClass) {
        this.theEntityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(Table entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Table entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Table entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Table find(Object id) {
        return getEntityManager().find(theEntityClass, id);
    }

    public List<Table> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(theEntityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<Table> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(theEntityClass));
        javax.persistence.Query query = getEntityManager().createQuery(cq);
        query.setMaxResults(range[1] - range[0] + 1);
        query.setFirstResult(range[0]);
        return query.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<Table> rt = cq.from(theEntityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query query = getEntityManager().createQuery(cq);
        return ((Long) query.getSingleResult()).intValue();
    }
    
}
