package session;

import entity.Booktable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daniel
 */
@Stateless
public class BooktableFacade extends AbstractTableFacade<Booktable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BooktableFacade() {
        super(Booktable.class);
    }
    
}
