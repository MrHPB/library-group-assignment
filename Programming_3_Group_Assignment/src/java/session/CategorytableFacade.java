package session;

import entity.Categorytable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daniel
 */
@Stateless
public class CategorytableFacade extends AbstractTableFacade<Categorytable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategorytableFacade() {
        super(Categorytable.class);
    }
    
}
