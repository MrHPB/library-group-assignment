package session;

import entity.Loantable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daniel
 */
@Stateless
public class LoantableFacade extends AbstractTableFacade<Loantable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LoantableFacade() {
        super(Loantable.class);
    }
    
}
