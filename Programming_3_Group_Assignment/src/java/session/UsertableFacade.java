package session;
import entity.Usertable;
import javax.ejb.Stateless;
import javax.persistence.*;

/**
 *
 * @author David
 */
@Stateless
public class UsertableFacade extends AbstractTableFacade<Usertable> { 
    private EntityManager em;
    @PersistenceContext(unitName = "libraryPU")
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    public UsertableFacade() {
        super(Usertable.class);
    }
    
}
