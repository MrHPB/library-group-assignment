package session;
import entity.Itemtable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author David
 */
@Stateless
public class ItemtableFacade extends AbstractTableFacade<Itemtable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ItemtableFacade() {
        super(Itemtable.class);
    }
    
}
