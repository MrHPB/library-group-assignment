package session;

import entity.Filmtable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daniel
 */
@Stateless
public class FilmtableFacade extends AbstractTableFacade<Filmtable> {
    @PersistenceContext(unitName = "libraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FilmtableFacade() {
        super(Filmtable.class);
    }
    
}
