<%-- 
    Document   : index
    Created on : Oct 09, 2018, 01:37:00 AM
    Author     : David, Libby
--%>

<%@page import="database.Database"%>
<%@page import="entity.Usertable"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<sql:setDataSource 
    var="datasource"
    driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/librarydatabase"
user="root" password="<%= Database.PASSWORD%>"
    />

<html>
    <!-- div id="indexRightColumn"-->

    <!--/div-->

    <body>
        <h1>FED UNI LIBRARY
        </h1>


        <h2>NEWS</h2>

        <div id="title">

            Federation University  is a university <br>
        </div>

        <div>               
            <h3>Latest Additions</h3>

            <div>
                <sql:query var="listItems"   dataSource="${datasource}">
                    SELECT *
                    FROM itemtable
                    ORDER BY ItemID DESC LIMIT 5;

                </sql:query> 

                <div align="center">      
                    <table id="itemTable" border="1" cellpadding="5">
                        <tr>
                            <th>Title</th>
                            <th>Item Type</th>
                            <th>Loaned?</th>
                            <th>Item Page</th>

                        </tr>

                        <c:forEach var="item" items="${listItems.rows}">

                            <tr>
                                <td><c:out value="${item.Title}" /></td> 
                                <td align="center"><c:out value="${item.ItemType}" /></td> 
                                <td align="center"><c:out value="${item.Loan}" /></td> 
                                <td align="center">
                            <form action="itemPage.jsp" method="get">
                                <input type="hidden" name="type" value="<c:out value="${item.ItemType}" />"/>
                                <input type="hidden" name="id" value="<c:out value="${item.ItemID}" />"/>
                                 <button class="goButton" type="submit"><span>Go</span></button>
                            </form>
                        </td> 
                            </tr>
                        </c:forEach>
                    </table>
                
                <!-- Dan - Changed the following table so that it sits in the same 
                    div as the previous table and is therefore center-aligned -->
                
                <h3>Opening Hours</h3>

                <table id="itemTable" border="1" cellpadding="5">
                    <tr>
                        <th>Location</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                        <th>Sunday</th>
                    </tr>

                    <tr>
                        <td>Mt Helen</td>
                        <td>8:30am - 8:00pm</td>
                        <td>8:30am - 8:00pm</td>
                        <td>8:30am - 8:00pm</td>
                        <td>8:30am - 8:00pm</td>
                        <td>8:30am - 8:00pm</td>
                        <td>9:00am - 4:00pm</td>
                        <td>9:00am - 4:00pm</td>
                    </tr

                    <tr>
                        <td>Berwick</td>
                        <td>8:30am - 7:00pm</td>
                        <td>8:30am - 7:00pm</td>
                        <td>8:30am - 7:00pm</td>
                        <td>8:30am - 7:00pm</td>
                        <td>8:30am - 6:00pm</td>
                        <td>10:00am - 3:00pm</td>
                        <td>10:00am- 3:00pm</td>
                    </tr>
                </table>
                </div></div>

        </div>

    </body>
</html>
