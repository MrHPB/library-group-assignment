<%-- 
    Document   : logoff
    Created on : 14/10/2018, 06:04:50 PM
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logging off</title>
    </head>
    <body>
        <% session.invalidate();
        response.sendRedirect("index.jsp");             %>
        
    </body>
</html>
