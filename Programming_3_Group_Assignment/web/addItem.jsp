<%-- 
    Document   : addItem
    Created on : Oct 16, 2018, 12:19:09 PM
    Author     : Libby
--%>
<%@page import="database.Database"%>
<%@page import="entity.Usertable"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>




<sql:setDataSource 
    var="datasource"
    driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/librarydatabase"
user="root" password="<%= Database.PASSWORD%>"
    />

<div align="center">

    <h3>Item Addition</h3>

    <div data-tabs="1" id="tabs">
        <nav data-tab-container="1" class="tab-container">

            <a href="#book-tab" data-tab="1" class="tab">Books</a>

            <a href="#cd-tab" data-tab="1" class="tab">CDs</a>

            <a href="#film-tab" data-tab="1" class="tab">Films</a>
        </nav>

        <%-- mainControllerSrv add each item POST --%>

        <div id="book-tab" data-tab-content="1" class="tab-content">
            <%-- 
                X`ItemID` int(8) NOT NULL AUTO_INCREMENT,
                `Title` varchar(50) NOT NULL DEFAULT '',
                `ItemType` varchar(20) NOT NULL DEFAULT '',
                `CategoryID` int(8) NOT NULL,
                `Description` varchar(50) NOT NULL DEFAULT '',
                `Loan` tinyint(1) NOT NULL DEFAULT '0',
                `Author` varchar(25) NOT NULL DEFAULT '',
                `Publisher` varchar(25) NOT NULL DEFAULT '',
                `PublishDate` date NOT NULL DEFAULT '1901-01-01',
                `ISBN` varchar(10) NOT NULL DEFAULT '',
            --%>


            <form action="addItem" method="post">
                <table border="0" cellpadding="5" align="center">
                    <tbody>
                        <tr>
                            <td><label>Title</label></td>
                            <td><input type="text" name="title" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Category</label></td>
                            <td><select name="categoryID" size="1">
                                    <sql:query var="listItems"   dataSource="${datasource}">
                                        SELECT *
                                        FROM categorytable ; 
                                    </sql:query>
                                    <c:forEach var="item" items="${listItems.rows}">
                                        <option value="<c:out value="${item.CategoryID}"/>"> <c:out value="${item.CategoryType}"/> </option>
                                    </c:forEach>
                                </select>                
                            </td>
                        </tr>
                        <tr>
                            <td><label>Description</label></td>
                            <td><textarea name="description" rows="5" cols="20"></textarea></td>
                        </tr>
                        <tr>
                            <td><label>Author</label></td>
                            <td><input type="text" name="author" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Publisher</label></td>
                            <td><input type="text" name="publisher" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Publish Date</label></td>
                            <td><input type="date" name="publishDate" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>ISBN</label></td>
                            <td><input type="text" name="isbn" size="25" /></td>
                        </tr>
                    </tbody>
                </table>
                <p>
                    <sql:query var="listItems"   dataSource="${datasource}">
                        SELECT ItemID 
                        FROM itemtable 
                        ORDER BY ItemID DESC LIMIT 1; 

                    </sql:query> 
                    <c:forEach var="item" items="${listItems.rows}">

                    <input type="hidden" name="itemID" value="<c:out value="${item.ItemID+1}" />"/>
                </c:forEach>

                <input type="hidden" name ="itemType" value="Book" />
                <input type="submit" value="Submit" />
                <input type="reset" value="Reset" />
                </p>
            </form>
        </div>
        <div id="cd-tab" data-tab-content="1" class="tab-content">
            <%-- 
                `ItemID` int(8) NOT NULL AUTO_INCREMENT,
                `Title` varchar(50) NOT NULL DEFAULT '',
                `ItemType` varchar(20) NOT NULL DEFAULT '',
                `CategoryID` int(8) NOT NULL,
                `Description` varchar(50) NOT NULL DEFAULT '',
                `Loan` tinyint(1) NOT NULL DEFAULT '0',
                `Artist` varchar(50) NOT NULL DEFAULT '',
                `ReleaseDate` date NOT NULL DEFAULT '1901-01-01',
            --%>
            <form action="addItem" method="post">
                <table border="0" cellpadding="5" align="center">
                    <tbody>



                        <tr>
                            <td><label>Title</label></td>
                            <td><input type="text" name="title" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Category</label></td>
                            <td><select name="categoryID" size="1">
                                    <sql:query var="listItems"   dataSource="${datasource}">
                                        SELECT *
                                        FROM categorytable ; 
                                    </sql:query>
                                    <c:forEach var="item" items="${listItems.rows}">
                                      <option value="<c:out value="${item.CategoryID}"/>"> <c:out value="${item.CategoryType}"/> </option>

                                    </c:forEach>
                                </select>                
                            </td>
                        </tr>
                        <tr>
                            <td><label>Description</label></td>
                            <td><input type="textarea" name="description" rows="6" cols="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Artist</label></td>
                            <td><input type="text" name="artist" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Release Date</label></td>
                            <td><input type="date" name="releaseDate" size="25" /></td>
                        </tr>
                    </tbody>
                </table>
                  <p>
                    <sql:query var="listItems"   dataSource="${datasource}">
                        SELECT ItemID 
                        FROM itemtable 
                        ORDER BY ItemID DESC LIMIT 1; 

                    </sql:query> 
                    <c:forEach var="item" items="${listItems.rows}">

                    <input type="hidden" name="itemID" value="<c:out value="${item.ItemID+1}" />"/>
                </c:forEach>

                <input type="hidden" name ="itemType" value="CD" />
                <input type="submit" value="Submit" />
                <input type="reset" value="Reset" />
                </p>
            </form>
        </div>
        <div id="film-tab" data-tab-content="1" class="tab-content">
            <%-- 
                `ItemID` int(8) NOT NULL AUTO_INCREMENT,
                `Title` varchar(50) NOT NULL DEFAULT '',
                `ItemType` varchar(20) NOT NULL DEFAULT '',
                `CategoryID` int(8) NOT NULL,
                `Description` varchar(50) NOT NULL DEFAULT '',
                `Loan` tinyint(1) NOT NULL DEFAULT '0',
                `Director` varchar(50) NOT NULL DEFAULT '',
                `ReleaseDate` date NOT NULL DEFAULT '1901-01-01',
            --%>
            <form action="addItem" method="post">
                <table border="0" cellpadding="5" align="center">
                    <tbody>
                        <tr>
                            <td><label>Title</label></td>
                            <td><input type="text" name="title" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Category</label></td>
                            <td><select name="categoryID" size="1">
                                    <sql:query var="listItems"   dataSource="${datasource}">
                                        SELECT *
                                        FROM categorytable ; 
                                    </sql:query>
                                    <c:forEach var="item" items="${listItems.rows}">
                                        <option value="<c:out value="${item.CategoryID}"/>"> <c:out value="${item.CategoryType}"/> </option>

                                    </c:forEach>
                                </select>                
                            </td>
                        </tr>
                        <tr>
                            <td><label>Description</label></td>
                            <td><input type="textarea" name="description" rows="6" cols="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Director</label></td>
                            <td><input type="text" name="director" size="25" /></td>
                        </tr>
                        <tr>
                            <td><label>Release Date</label></td>
                            <td><input type="date" name="releaseDate" size="25" /></td>
                        </tr>
                    </tbody>
                </table>
                 <p>
                    <sql:query var="listItems"   dataSource="${datasource}">
                        SELECT ItemID 
                        FROM itemtable 
                        ORDER BY ItemID DESC LIMIT 1; 

                    </sql:query> 
                    <c:forEach var="item" items="${listItems.rows}">

                    <input type="hidden" name="itemID" value="<c:out value="${item.ItemID+1}" />"/>
                </c:forEach>

                <input type="hidden" name ="itemType" value="Film" />
                <input type="submit" value="Submit" />
                <input type="reset" value="Reset" />
                </p>
            </form>
        </div>
    </div>
</div>
<script>
    var items = document.querySelectorAll('#tabs');
    for (var i = 0, len = items.length; i < len; i++) {
        (function () {
            var t, e = this,
                    a = "[data-tab]",
                    n = document.body,
                    r = n.matchesSelector || n.webkitMatchesSelector || n.mozMatchesSelector || n.msMatchesSelector,
                    o = function () {
                        var a = e.querySelectorAll("[data-tab-content]") || [];
                        for (t = 0; t < a.length; t++)
                            a[t].style.display = "none"
                    },
                    i = function (n) {
                        var r = e.querySelectorAll(a) || [];
                        for (t = 0; t < r.length; t++) {
                            var i = r[t],
                                    s = i.className.replace("tab-active", "").trim();
                            i.className = s
                        }
                        o(), n.className += " tab-active";
                        var l = n.getAttribute("href"),
                                c = e.querySelector(l);
                        c && (c.style.display = "")
                    },
                    s = e.querySelector(".tab-active" + a);
            s = s || e.querySelector(a), s && i(s), e.addEventListener("click", function (t) {
                var e = t.target;
                r.call(e, a) && i(e)
            })
        }.bind(items[i]))();
    }

</script>
