<%-- 
    Document   : login
    Created on : Oct 09, 2018, 03:00:35 PM
    Author     : David
--%>

<p></p>

<div style="color:red">${errorMessage}</div>


<form action="LoginSrv" method ="POST">
    <p>
        Please enter your login details below.
    </p>
    <table border="0" cellpadding="5" align="center">
        <tbody>
            <tr>
                <td><label>Email</label></td>
                <td><input type="text" name="Email" size="20" /></td>
            </tr>
            <tr>
                <td><label>Password</label></td>
                <td><input type="password" name="Password" size="20" /></td>
            </tr>
        </tbody>
    </table>

    <p>
        <button class="subButton" type="submit"><span>Submit</span></button>
    </p>
</form>
