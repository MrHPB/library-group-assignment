<%-- 
    Document   : accountPage
    Created on : Oct 14, 2018, 06:30:09 PM
    Authors     : David, Libby
--%>
<%@page import="database.Database"%>
<%@page import="entity.Usertable"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<%! Usertable user;%> 

<%        user = (Usertable) request.getSession().getAttribute("user");
    if (session.getAttribute("user") == null) {
        out.println("user is not logged in, re-directing to login page");
        response.sendRedirect("login.jsp");
    }
%>         


<sql:setDataSource 
    var="datasource"
    driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/librarydatabase"
user="root" password="<%= Database.PASSWORD%>"
    />

<div align="center">

    <h3><% out.print(user.getName()); %>'s account page</h3>

    <div data-tabs="1" id="tabs">
        <nav data-tab-container="1" class="tab-container">
            <a href="#loan-tab" data-tab="1" class="tab">My Loans</a>

            <a href="#request-tab" data-tab="1" class="tab">My Requests</a>

            <a href="#comment-tab" data-tab="1" class="tab">My Comments</a>

        </nav>

        <div id="loan-tab" data-tab-content="1" class="tab-content">

            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT i.ItemID, i.Title
                FROM itemtable i 
                JOIN loantable l 
                ON l.ItemID=i.ItemID 
                WHERE l.UserID= <% out.println(user.getUserID()); %> ; 

            </sql:query> 

            <div align="center">      
                <table id="loanTable" border="1" cellpadding="5">
                    <caption><h4>Current Loans</h4></caption>
                    <tr>
                        <th>Item ID</th>
                        <th>Title</th>
                        <th>Return</th>


                    </tr>

                    <c:forEach var="item" items="${listItems.rows}">

                        <tr>
                            <td><c:out value="${item.ItemID}" /></td>
                            <td><c:out value="${item.Title}" /></td> 
                            <td>
                                <form action="return" method="POST">
                                <input type="hidden" name="itemId" value="${item.ItemID}">
                                <button class="goButton" type="submit"><span>Return</span></button>
                                </form>           
                            </td>   
                        </tr>
                    </c:forEach>
                </table>
            </div>   	

        </div>
        <div id="request-tab" data-tab-content="1" class="tab-content">

            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT i.ItemID, i.Title
                FROM requesttable b 
                JOIN itemtable i 
                ON b.ItemID=i.ItemID 
                WHERE b.UserID= <% out.println(user.getUserID()); %> ; 

            </sql:query> 

            <div align="center">
                <table id="requestTable" border="1" cellpadding="5">
                    <caption><h4>Current Requests</h4></caption>
                    <tr>
                        <th>Item ID</th>
                        <th>Title</th>


                    </tr>

                    <c:forEach var="item" items="${listItems.rows}">

                        <tr>
                            <td><c:out value="${item.ItemID}" /></td>
                            <td><c:out value="${item.Title}" /></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>	

        </div>
        <div id="comment-tab" data-tab-content="1" class="tab-content">

            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT p.content, p.ItemID,
                (SELECT Title FROM itemtable i WHERE i.ItemID=p.ItemID) as Title  
                FROM commenttable p 
                WHERE p.UserID= <% out.println(user.getUserID());%> ; 

            </sql:query> 

            <div align="center">
                <table id="commentTable" border="1" cellpadding="5">
                    <caption><h4>My Comments</h4></caption>
                    <tr>
                        <th>Item ID</th>
                        <th>Title</th>
                        <th>Comment</th>

                    </tr>

                    <c:forEach var="item" items="${listItems.rows}">

                        <tr>
                            <td><c:out value="${item.ItemID}" /></td> 
                            <td><c:out value="${item.Title}" /></td> 
                            <td><c:out value="${item.content}" /></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>	

        </div>
    </div>

</div>

<script>
			var items = document.querySelectorAll('#tabs');
			for (var i = 0, len = items.length; i < len; i++) {
			(function() {
				var t, e = this,
				a = "[data-tab]",
				n = document.body,
				r = n.matchesSelector || n.webkitMatchesSelector || n.mozMatchesSelector || n.msMatchesSelector,
				o = function() {
					var a = e.querySelectorAll("[data-tab-content]") || [];
					for (t = 0; t < a.length; t++) a[t].style.display = "none"
				},
				i = function(n) {
					var r = e.querySelectorAll(a) || [];
					for (t = 0; t < r.length; t++) {
						var i = r[t],
						s = i.className.replace("tab-active", "").trim();
						i.className = s
					}
					o(), n.className += " tab-active";
					var l = n.getAttribute("href"),
					c = e.querySelector(l);
					c && (c.style.display = "")
				},
				s = e.querySelector(".tab-active" + a);
				s = s || e.querySelector(a), s && i(s), e.addEventListener("click", function(t) {
				var e = t.target;
				r.call(e, a) && i(e)
				})
			}.bind(items[i]))();
			}

		</script>






