<%-- 
    Document   : items
    Created on : Oct 16, 2018, 12:19:09 PM
    Author     : Libby and David
--%>
<%@page import="database.Database"%>
<%@page import="entity.Usertable"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<% String itemId = (String) session.getAttribute("id"); %>

<sql:setDataSource 
    var="datasource"
    driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/librarydatabase"
user="root" password="<%= Database.PASSWORD%>"
    />



<h3>Library Item</h3>

<div align="center">      
    <table id="itemTable" border="1" cellpadding="5">


        <c:set var="type" value="<%= request.getParameter("type")%>"/>

        <c:if test="${(type == 'Book')}">
            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable i 
                JOIN booktable l 
                ON l.ItemID=i.ItemID 
                WHERE i.ItemID = <%= request.getParameter("id")%>
                ; 
            </sql:query>
            <c:forEach var="item" items="${listItems.rows}">         
                <tbody >

                    <tr>
                        <td ><label>ID</label></td>
                        <td ><c:out value="${item.ItemID}" /></td>
                    </tr>
                    <tr>
                        <td><label>Title</label></td>
                        <td><c:out value="${item.Title}" /></td>
                    </tr>
                    <tr>
                        <td><label>Item Type</label></td>
                        <td><c:out value="${item.ItemType}" /></td>
                    </tr>
                    <tr>
                        <td><label>Loan Status</label></td>
                        <td><c:out value="${item.Loan}" /></td>
                    </tr>
                    <tr>
                        <td><label>Category</label></td>
                        <td><c:out value="${item.CategoryID}" /></td>
                    </tr>
                    <tr>
                        <td><label>Description</label></td>
                        <td width="250px"><c:out value="${item.Description}" /></td>
                    </tr>
                    <tr>
                        <td><label>Author</label></td>
                        <td><c:out value="${item.Author}" /></td>
                    </tr>
                    <tr>
                        <td><label>Publisher</label></td>
                        <td><c:out value="${item.Publisher}" /></td>
                    </tr>
                    <tr>
                        <td><label>Publish Date</label></td>
                        <td><c:out value="${item.PublishDate}" /></td>
                    </tr>
                    <tr>
                        <td><label>ISBN</label></td>
                        <td><c:out value="${item.ISBN}" /></td>
                    </tr>
                </tbody>

            </c:forEach>

        </c:if>
        <c:if test="${(type == 'CD')}">
            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable i 
                JOIN cdtable l 
                ON l.ItemID=i.ItemID 
                WHERE i.ItemID = <%= request.getParameter("id")%>
                ;
            </sql:query>
            <c:forEach var="item" items="${listItems.rows}">       
                <tbody>

                    <tr>
                        <td ><label>ID</label></td>
                        <td ><c:out value="${item.ItemID}" /></td>
                    </tr>
                    <tr>
                        <td><label>Title</label></td>
                        <td><c:out value="${item.Title}" /></td>
                    </tr>
                    <tr>
                        <td><label>Item Type</label></td>
                        <td><c:out value="${item.ItemType}" /></td>
                    </tr>
                    <tr>
                        <td><label>Loan Status</label></td>
                        <td><c:out value="${item.Loan}" /></td>
                    </tr>
                    <tr>
                        <td><label>Category</label></td>
                        <td><c:out value="${item.CategoryID}" /></td>
                    </tr>
                    <tr>
                        <td><label>Description</label></td>
                        <td width="250px"><c:out value="${item.Description}" /></td>
                    </tr>
                    <tr>
                        <td><label>Artist</label></td>
                        <td><c:out value="${item.Artist}" /></td>
                    </tr>
                    <tr>
                        <td><label>Release Date</label></td>
                        <td><c:out value="${item.ReleaseDate}" /></td>
                    </tr>
                </tbody>

            </c:forEach>

        </c:if>
        <c:if test="${(type == 'Film')}">
            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable i 
                JOIN filmtable l 
                ON l.ItemID=i.ItemID 
                WHERE i.ItemID = <%= request.getParameter("id")%>
                ;
            </sql:query>
            <c:forEach var="item" items="${listItems.rows}">      
                <tbody>

                    <tr>
                        <td ><label>ID</label></td>
                        <td ><c:out value="${item.ItemID}" /></td>
                    </tr>
                    <tr>
                        <td><label>Title</label></td>
                        <td><c:out value="${item.Title}" /></td>
                    </tr>
                    <tr>
                        <td><label>Item Type</label></td>
                        <td><c:out value="${item.ItemType}" /></td>
                    </tr>
                    <tr>
                        <td><label>Loan Status</label></td>
                        <td><c:out value="${item.Loan}" /></td>
                    </tr>
                    <tr>
                        <td><label>Category</label></td>
                        <td><c:out value="${item.CategoryID}" /></td>
                    </tr>
                    <tr>
                        <td><label>Description</label></td>
                        <td width="250px"><c:out value="${item.Description}" /></td>
                    </tr>
                    <tr>
                        <td><label>Director</label></td>
                        <td><c:out value="${item.Director}" /></td>
                    </tr>
                    <tr>
                        <td><label>Release Date</label></td>
                        <td><c:out value="${item.ReleaseDate}" /></td>
                    </tr>
                </tbody>

            </c:forEach>

        </c:if>

    </table>
</div>

<%
    if (request.getSession().getAttribute("user") != null) {
        user = (Usertable) request.getSession().getAttribute("user");
        %>
         <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable  
                WHERE ItemID = <%= request.getParameter("id")%>
                ;
            </sql:query>
                <c:forEach var="item" items="${listItems.rows}">      
                <%
        if (user.isAdmin()) { %>
            <td>
                <form action="delete" method="POST">
                    <input type="hidden" name="itemId" value="${item.ItemId}">
                    <button class="goButton" type="submit"><span>Delete</span></button>
                </form>       
            </td>   
            <td>
                <form action="return" method="POST">
                    <input type="hidden" name="itemId" value="${item.ItemId}">
                    <button class="goButton" type="submit"><span>Return Override</span></button>
                </form>           
            </td>   
<%
             } 
             if (!user.isAdmin()) { %>
            <c:if test="${(item.Loan == 'false')}">
     

                        <p>
                        <form action="itemPage" method="POST" name="addLoan">
                            <input type="hidden" value="<%= request.getParameter("id")%>" name="itemId" />
                            <input type="hidden" value="${item.Loan}" name="loaned" />
                            <button type="submit" Value="submit" name="addLoan"> Loan Item </button>
                        </form>
                        </p>
    </c:if>
              <c:if test="${(item.Loan  == 'true')}">
               

                        <p>
                        <form action="itemPage" method="POST" name="addLoan">
                            <input type="hidden" value="<%= request.getParameter("id")%>" name="itemId" />
                            <input type="hidden" value="${item.Loan}" name="loaned" />
                            <button type="submit" Value="submit" name="addLoan">Request Item</button>
                        </form>
                        </p>

   </c:if>
              <%
                    } %>
</c:forEach>           

                     <%
                    }
%>   


<p><h3>Comments</h3></p>

<sql:query var="commentList"   dataSource="${datasource}">
    SELECT * FROM commenttable WHERE ItemID = <%= request.getParameter("id")%>;
</sql:query> 
<c:forEach var="comment" items="${commentList.rows}">
    <sql:query var="userList" dataSource="${datasource}">
        SELECT * FROM usertable WHERE UserId = "${comment.UserID}";
    </sql:query>
    <p>
        <c:forEach var="user" items="${userList.rows}">
            <c:out value="${user.Name}" />
    </c:forEach> 
    ${comment.Content}
</p>

</c:forEach>

<%
    if (request.getSession().getAttribute("user") != null) {
%>
<div>
    <table align="center">
        <form action="itemPage" method="POST" name="addComment">
            <tr>
                <td><textarea type="text" name="comment" rows="5" cols="50" placeholder="Post a comment">
                    </textarea></td>
            </tr>
            <tr> <td align="center">
                    <input type="hidden" name="itemId" value="<%= request.getParameter("id")%>">
                    <button type="submit" Value="submit" name="addComment"> Add Comment </button>
                </td></tr>
        </form>
    </table>
</div>

<%
    }
%>





