<%-- 
    Document   : search
    Created on : Oct 16, 2018, 12:19:09 PM
    Author     : David and Libby
--%>

<%@page import="database.Database"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<sql:setDataSource 
    var="datasource"
    driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/libraryDatabase"
user="root" password="<%= Database.PASSWORD%>"
    />

<sql:query var="listItems"   dataSource="${datasource}">
    SELECT * FROM itemtable 
    WHERE Title like ?;

    <sql:param value="%${title}%"/>

</sql:query>

<p></p>
<div align="center">
    <h2>Search for an Item</h2>
</div>
<form action="search" method="get">

    <table border="0" cellpadding="5" align="center">
        <tbody>
            <tr>
                <td><label>Search for title</label></td>
                <td><input type="text" name="title" size="20" /></td>
            </tr>

        </tbody>
    </table>
    <p> *blank submit will show all items* </p>
    <p>
        <button class="subButton" type="submit"><span>Submit</span></button>
    </p>
</form>

<div align="center">
    <table id="itemTable" border="1" cellpadding="5">
        <caption><h2>Search Results</h2></caption>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Item Type</th>
            <th>Category</th>
            <th>Description</th>
            <th>Loaned</th>
            <th>Item Page</th>
                <%
                    if (request.getSession().getAttribute("user") != null) {
                        user = (Usertable) request.getSession().getAttribute("user");
                        if (user.isAdmin()) { %>
            <th>Delete Item</th>
                <% }
                 } %>

        </tr>

        <c:forEach var="item" items="${listItems.rows}">

            <sql:query var="categoryList" dataSource="${datasource}">
                SELECT * FROM categorytable WHERE CategoryID = "${item.CategoryID}";
            </sql:query>

            <tr>
                <td align="center"><c:out value="${item.ItemID}" /></td>
                <td><c:out value="${item.Title}" /></td> 
                <td align="center"><c:out value="${item.ItemType}" /></td>
                <td align="center"><c:forEach var="category" items="${categoryList.rows}"><c:out value="${category.Categorytype}" /></c:forEach></td>
                <td><c:out value="${item.Description}" /></td>

                <td align="center"><c:out value="${item.loan}" /></td>
                <td align="center">
                    <form action="itemPage.jsp" method="get">
                        <input type="hidden" name="type" value="<c:out value="${item.ItemType}" />"/>
                        <input type="hidden" name="id" value="<c:out value="${item.ItemID}" />"/>
                        <button class="goButton" type="submit"><span>Go</span></button>
                    </form>
                </td>

                <%
                    if (request.getSession().getAttribute("user") != null) {
                        user = (Usertable) request.getSession().getAttribute("user");
                                 if (user.isAdmin()) { %>
                <td>
                    <form action="delete" method="POST">
                        <input type="hidden" name="itemId" value="${item.ItemId}">
                        <button class=goButton" type="submit"><span>"Delete"</span></button>
                    </form>
                </td>   
                <% }
                            }%>







            </tr>
        </c:forEach>
    </table>


