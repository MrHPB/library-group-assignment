<%-- 
    Document   : items
    Created on : Oct 16, 2018, 12:19:09 PM
    Author     : Libby
--%>
<%@page import="database.Database"%>
<%@page import="entity.Usertable"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>




<sql:setDataSource 
    var="datasource"
    driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/librarydatabase"
user="root" password="<%= Database.PASSWORD%>"
    />

<div align="center">

    <h3>Library Items</h3>

    <div data-tabs="1" id="tabs">
        <nav data-tab-container="1" class="tab-container">
            <a href="#all-tab" data-tab="1" class="tab">All Items</a>

            <a href="#book-tab" data-tab="1" class="tab">Books</a>

            <a href="#cd-tab" data-tab="1" class="tab">CDs</a>

            <a href="#film-tab" data-tab="1" class="tab">Films</a>
        </nav>

        <div id="all-tab" data-tab-content="1" class="tab-content">

            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT * 
                FROM itemtable ; 
            </sql:query>

            <div align="center">      
                <table class="table" id="itemTable" border="1" cellpadding="5">
                    <caption><h4>All Items</h4></caption>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Item Type</th>
                        <th>Loaned?</th>
                        <th>Item Page</th>

                    </tr>

                    <c:forEach var="item" items="${listItems.rows}">

                        <tr>
                            <td align="center"><c:out value="${item.ItemID}" /></td> 
                            <td><c:out value="${item.Title}" /></td> 
                            <td align="center"><c:out value="${item.ItemType}" /></td> 
                            <td align="center"><c:out value="${item.Loan}" /></td> 
                            <td align="center">
                                    <form action="itemPage.jsp" method="get">
                                        <input type="hidden" name="type" value="<c:out value="${item.ItemType}" />"/>
                                        <input type="hidden" name="id" value="<c:out value="${item.ItemID}" />"/>
                                         <button class="goButton" type="submit"><span>Go</span></button>
                                    </form>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div> 



        </div>
        <div id="book-tab" data-tab-content="1" class="tab-content">

            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable i 
                JOIN booktable j 
                ON j.ItemID=i.ItemID ;

            </sql:query>  	

            <table class="table" id="itemTable" border="1" cellpadding="5">
                <caption><h4>Books</h4></caption>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN</th>
                    <th>Loaned?</th>
                    <th>Item Page</th>

                </tr>

                <c:forEach var="item" items="${listItems.rows}">

                    <tr>
                        <td align="center"><c:out value="${item.ItemID}" /></td> 
                        <td><c:out value="${item.Title}" /></td> 
                        <td><c:out value="${item.Author}" /></td>
                        <td align="center"><c:out value="${item.ISBN}" /></td> 
                        <td align="center"><c:out value="${item.Loan}" /></td> 
                        <td align="center">
                            <form action="itemPage.jsp" method="get">
                                <input type="hidden" name="type" value="<c:out value="${item.ItemType}" />"/>
                                <input type="hidden" name="id" value="<c:out value="${item.ItemID}" />"/>
                                 <button class="goButton" type="submit"><span>Go</span></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>

        </div>
        <div id="cd-tab" data-tab-content="1" class="tab-content">
            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable i 
                JOIN cdtable j 
                ON j.ItemID=i.ItemID ;

            </sql:query> 

            <table class="table" id="itemTable" border="1" cellpadding="5">
                <caption><h4>CDs</h4></caption>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Artist</th>
                    <th>Release Date</th>
                    <th>Loaned?</th>
                    <th>Item Page</th>

                </tr>

                <c:forEach var="item" items="${listItems.rows}">

                    <tr>
                        <td align="center"><c:out value="${item.ItemID}" /></td> 
                        <td><c:out value="${item.Title}" /></td> 
                        <td><c:out value="${item.Artist}" /></td>
                        <td align="center"><c:out value="${item.ReleaseDate}" /></td> 
                        <td align="center"><c:out value="${item.Loan}" /></td> 
                        <td align="center">
                            <form action="itemPage.jsp" method="get">
                                <input type="hidden" name="type" value="<c:out value="${item.ItemType}" />"/>
                                <input type="hidden" name="id" value="<c:out value="${item.ItemID}" />"/>
                                 <button class="goButton" type="submit"><span>Go</span></button>
                            </form>
                        </td> 
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div id="film-tab" data-tab-content="1" class="tab-content">
            <sql:query var="listItems"   dataSource="${datasource}">
                SELECT *
                FROM itemtable i 
                JOIN filmtable j 
                ON j.ItemID=i.ItemID ;

            </sql:query> 

            <table class="table" id="itemTable" border="1" cellpadding="5">
                <caption><h4>Films</h4></caption>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Director</th>
                    <th>Release Date</th>
                    <th>Loaned?</th>
                    <th>Item Page</th>

                </tr>

                <c:forEach var="item" items="${listItems.rows}">

                    <tr>
                        <td align="center"><c:out value="${item.ItemID}" /></td> 
                        <td><c:out value="${item.Title}" /></td> 
                        <td><c:out value="${item.Director}" /></td>
                        <td align="center"><c:out value="${item.ReleaseDate}" /></td> 
                        <td align="center"><c:out value="${item.Loan}" /></td> 
                        <td align="center">
                            <form action="itemPage.jsp" method="get">
                                <input type="hidden" name="type" value="<c:out value="${item.ItemType}" />"/>
                                <input type="hidden" name="id" value="<c:out value="${item.ItemID}" />"/>
                                <button class="goButton" type="submit"><span>Go</span></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>

</div>



<%-- 
SQL Queries
-- All --
<sql:query var="listItems"   dataSource="${datasource}">
        SELECT * 
        FROM itemtable ; 
</sql:query> 

-- Book --
<sql:query var="listItems"   dataSource="${datasource}">
        SELECT *
        FROM itemtable i 
        JOIN booktable j 
        ON j.ItemID=i.ItemID ;

</sql:query> 

-- CD --
<sql:query var="listItems"   dataSource="${datasource}">
        SELECT *
        FROM itemtable i 
        JOIN cdtable j 
        ON j.ItemID=i.ItemID ;

</sql:query> 

-- Film --
<sql:query var="listItems"   dataSource="${datasource}">
        SELECT *
        FROM itemtable i 
        JOIN filmtable j 
        ON j.ItemID=i.ItemID ;

</sql:query> 
--%>


</div>


<script>
    var items = document.querySelectorAll('#tabs');
    for (var i = 0, len = items.length; i < len; i++) {
        (function () {
            var t, e = this,
                    a = "[data-tab]",
                    n = document.body,
                    r = n.matchesSelector || n.webkitMatchesSelector || n.mozMatchesSelector || n.msMatchesSelector,
                    o = function () {
                        var a = e.querySelectorAll("[data-tab-content]") || [];
                        for (t = 0; t < a.length; t++)
                            a[t].style.display = "none"
                    },
                    i = function (n) {
                        var r = e.querySelectorAll(a) || [];
                        for (t = 0; t < r.length; t++) {
                            var i = r[t],
                                    s = i.className.replace("tab-active", "").trim();
                            i.className = s
                        }
                        o(), n.className += " tab-active";
                        var l = n.getAttribute("href"),
                                c = e.querySelector(l);
                        c && (c.style.display = "")
                    },
                    s = e.querySelector(".tab-active" + a);
            s = s || e.querySelector(a), s && i(s), e.addEventListener("click", function (t) {
                var e = t.target;
                r.call(e, a) && i(e)
            })
        }.bind(items[i]))();
    }

</script>
<%--
-- Book --
<table id="itemTable" border="1" cellpadding="5">
        <caption><h4>Books</h4></caption>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>ISBN</th>
            <th>Loan</th>

        </tr>

        <c:forEach var="item" items="${listItems.rows}">

            <tr>
                <td><c:out value="${item.ItemID}" /></td> 
                <td><c:out value="${item.Title}" /></td> 
                <td><c:out value="${item.Author}" /></td>
                <td><c:out value="${item.ISBN}" /></td> 
                <td><c:out value="${item.Loan}" /></td> 
            </tr>
        </c:forEach>
    </table>

-- CD --
<table id="itemTable" border="1" cellpadding="5">
        <caption><h4>Books</h4></caption>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Artist</th>
            <th>ISBN</th>
            <th>Loan</th>

        </tr>

        <c:forEach var="item" items="${listItems.rows}">

            <tr>
                <td><c:out value="${item.ItemID}" /></td> 
                <td><c:out value="${item.Title}" /></td> 
                <td><c:out value="${item.Artist}" /></td>
                <td><c:out value="${item.ISBN}" /></td> 
                <td><c:out value="${item.Loan}" /></td> 
            </tr>
        </c:forEach>
    </table>

-- Film --
<table id="itemTable" border="1" cellpadding="5">
        <caption><h4>Books</h4></caption>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Director</th>
            <th>ISBN</th>
            <th>Loan</th>

        </tr>

        <c:forEach var="item" items="${listItems.rows}">

            <tr>
                <td><c:out value="${item.ItemID}" /></td> 
                <td><c:out value="${item.Title}" /></td> 
                <td><c:out value="${item.Director}" /></td>
                <td><c:out value="${item.ISBN}" /></td> 
                <td><c:out value="${item.Loan}" /></td> 
            </tr>
        </c:forEach>
    </table>
--%>




