<%-- 
    Document   : registerAccount
    Created on : Oct 14, 2018, 06:50:35 PM
    Author     : David
--%>

<p></p>

<div style="color:red">${errorMessage}</div>

<form action="registerAccount" method="post">
    
    <table border="0" cellpadding="5" align="center">
        <tbody>
            <tr>
                <td><label>Username</label></td>
                <td><input type="text" name="userName" size="25" /></td>
            </tr>
            <tr>
                <td><label>Email</label></td>
                <td><input type="text" name="email" size="25" /></td>
            </tr>
            <tr>
                <td><label>Phone Number</label></td>
                <td><input type="text" name="phoneNumber" size="25" /></td>
            </tr>
            <tr>
                <td><label>Password</label></td>
                <td><input type="password" name="password" size="25" /></td>
            </tr>
        </tbody>
    </table>

    <p>
       <button class="subButton" type="submit"><span>Submit</span></button>
        <button class="subButton" type="reset"><span>Reset</span></button>
    </p>
</form>
