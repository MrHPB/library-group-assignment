
-- Library Database: 'librarydatabase'
--
--
-- Contents -----------------------------------------------

-- Database Creation
-- Table Creation
-- Table Population 
-- Table Constraints

-- --------------------------------------------------------

-- Database Creation -----------------------------------------

DROP DATABASE IF EXISTS librarydatabase;

CREATE DATABASE librarydatabase CHARACTER SET utf8 COLLATE utf8_general_ci;

USE librarydatabase;

GRANT SELECT, INSERT, UPDATE, DELETE
  ON librarydatabase.*
  TO 'libraryUser'@'localhost'
  IDENTIFIED BY 'AxBkAJwMYCFj3RaGypHG'; 

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- --------------------------------------------------------

-- Table Creation -----------------------------------------

-- Table structure for table `usertable`

	CREATE TABLE IF NOT EXISTS `usertable` (
	  `UserID` int(8) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(15) NOT NULL DEFAULT '',
	  `Email` varchar(50) NOT NULL DEFAULT '',
	  `Phonenumber` varchar(12) NOT NULL DEFAULT '',
	  `Password` varchar(50) NOT NULL DEFAULT '',
	  `admin` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


-- Table structure for table `categorytable`

	CREATE TABLE IF NOT EXISTS `categorytable` (
	  `CategoryID` int(8) NOT NULL AUTO_INCREMENT,
	  `CategoryType` varchar(20) NOT NULL DEFAULT '', -- Type changed to upper case - Dan 23/10/18
	  `CategoryDesc` varchar(50) NOT NULL DEFAULT '',
	  PRIMARY KEY (`CategoryID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;


-- Table structure for table `itemtable`

	CREATE TABLE IF NOT EXISTS `itemtable` (
	  `ItemID` int(8) NOT NULL AUTO_INCREMENT,
	  `Title` varchar(50) NOT NULL DEFAULT '',
	  `ItemType` varchar(20) NOT NULL DEFAULT '',
	  `CategoryID` int(8) NOT NULL,
	  `Description` varchar(50) NOT NULL DEFAULT '',
	  `Loan` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`ItemID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;
	
	
-- Table structure for table `booktable`

	CREATE TABLE IF NOT EXISTS `booktable` (
	  `ItemID` int(8) NOT NULL AUTO_INCREMENT,
	  `Author` varchar(25) NOT NULL DEFAULT '',
	  `Publisher` varchar(25) NOT NULL DEFAULT '',
	  `PublishDate` date NOT NULL DEFAULT '1901-01-01',
	  `ISBN` varchar(10) NOT NULL DEFAULT '',
	  PRIMARY KEY (`ItemID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;
	
	
-- Table structure for table `cdtable`

	CREATE TABLE IF NOT EXISTS `cdtable` (
	  `ItemID` int(8) NOT NULL AUTO_INCREMENT,
	  `Artist` varchar(50) NOT NULL DEFAULT '',
	  `ReleaseDate` date NOT NULL DEFAULT '1901-01-01',
	  PRIMARY KEY (`ItemID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;
	
	
-- Table structure for table `filmtable`

	CREATE TABLE IF NOT EXISTS `filmtable` (
	  `ItemID` int(8) NOT NULL AUTO_INCREMENT,
	  `Director` varchar(50) NOT NULL DEFAULT '',
	  `ReleaseDate` date NOT NULL DEFAULT '1901-01-01',
	  PRIMARY KEY (`ItemID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;
	
	
	-- Table structure for table `loantable`

	CREATE TABLE IF NOT EXISTS `loantable` (
	  `LoanID` int(8) NOT NULL AUTO_INCREMENT,
	  `ItemID` int(8) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `ReturnDate` date NOT NULL DEFAULT '1901-01-01',
	  PRIMARY KEY (`LoanID`),
	  KEY `ItemID` (`ItemID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;



-- Table structure for table `commenttable`

	CREATE TABLE IF NOT EXISTS `commenttable` (
	  `CmntID` int(8) NOT NULL AUTO_INCREMENT,
	  `ItemID` int(8) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Content` varchar(50) NOT NULL DEFAULT '',
	  PRIMARY KEY (`CmntID`),
	  KEY `ItemID` (`ItemID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


-- Table structure for table `requesttable`

	CREATE TABLE IF NOT EXISTS `requesttable` (
	  `RqstID` int(8) NOT NULL AUTO_INCREMENT,		-- Added new Primary Key - Dan 23/10/18
	  `UserID` int(8) NOT NULL,
	  `ItemID` int(8) NOT NULL,
	  PRIMARY KEY (`RqstID`),
	  KEY `ItemID` (`ItemID`),
	  FOREIGN KEY (`UserID`) REFERENCES `usertable`(`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- Table Population ---------------------------------------

-- Content for table 'usertable'

	INSERT INTO `usertable` (`UserID`, `Name`, `Email`, `Phonenumber`, `Password`, `admin`) VALUES
	(1, 'admin', 'admin@hotmail.com', '12348765', 'password', 1),
	(2, 'user', 'user@hotmail.com', '12348765', 'password', 0);

-- Content for table 'categorytable'

	INSERT INTO `categorytable` (`CategoryID`, `Categorytype`, `CategoryDesc`) VALUES
	(1, 'Comedy', 'Mature audience'),
	(2, 'Horror', 'Dark themes'),
	(3, 'Action', 'Family Friendly'),
	(4, 'Drama', 'Suitable for teenagers'),
	(5, 'Educational', 'Backed by peer review'),
    (6, 'Mystery', 'Puzzlingly unsettled event'),
    (7, 'Fiction', 'Imaginative narration'),
    (8, 'Children', 'Kids Audience'),
    (9, 'Music', 'Art form of pitch, rhythm and its associated concepts');




-- Content for table 'itemtable'

	INSERT INTO `itemtable` (`ItemID`, `Title`, `ItemType`, `CategoryID`, `Description`, `Loan`) VALUES

	(1, 'Ottolenghi SIMPLE', 'Book', 5, 'Everything you love about Ottolenghi, made simple.', 1),
	(2, 'The Barefoot Investor', 'Book', 5, 'The Only Money Guide You will Ever Need',  1),
	(3, 'The Lost Man', 'Book', 4, 'Three brothers, one death, a fenceline stretching to the horizon.', 0),
	(4, 'The Outsider','Book', 3,  'Murder solving action', 1),
	(5, 'Life of Pi','Book', 7, 'Survivor: a fearsome Bengal Tiger', 0),
	(6, 'The President is Missing','Book', 6, 'Disappearance of the U.S President', 1),
    (7, 'Wake Up Jeff!','CD', 8, '6th album of The Wiggles', 0),
	(8, 'Kevs Krissmas - Volume 2','CD', 1, 'A perfect example of KBWs Master Craftmanship', 0),	
	(9, 'Life is Good','CD', 8, 'Childrens music', 1),	
	(10, 'Walk Between Worlds','CD', 9, 'Album form Simple Minds', 0),
	(11, 'Educational- Abc 123','CD', 8, 'Childrens Music', 1),	
	(12, 'My First Classical Music: The Complete Collection ','CD', 9, 'Imaginzation of classical music', 0),
	(13, 'Frankenstein','Film', 3, 'Mad Scientist who attempt to be perfect human', 1),	
	(14, 'The family Fang','Film', 6, 'Sibling of world famous parents who have disappeared', 0),
	(15, 'The Witch','Film', 7, 'Family forces of witchcraft, black magic and possession', 0),
	(16, 'Space station 76 ','Film', 5, 'A 1970s version of the future', 0),
    (17, 'Journey to the Center of the Earth','Film', 5, 'Discovering a fantastic the lost world in the centre of the earth', 1),
    (18, 'Bully','Film', 5, 'A documentary on peer-to-peer bullying in schools across America', 0),
	(19, 'Blue Disco','CD', 8, '6th album of The Wiggles', 0),
	(20, 'Kevs Krissmas - Volume 3','CD', 1, 'A perfect example of KBWs Master Craftmanship', 0),	
	(21, 'Life is Good 2','CD', 8, 'Childrens music', 0),	
	(22, 'Walk Between Worlds 2','CD', 9, 'Album form Simple Minds', 0),
	(23, 'Educational- Abc 123 more','CD', 8, 'Childrens Music', 0),	
	(24, 'My Second Classical Music: The Complete Collection ','CD', 9, 'Imaginzation of classical music', 0),
	(25, 'Frankenstein 2','Film', 3, 'Mad Scientist who attempt to be perfect human again', 0),	
	(26, 'The family Fang 2','Film', 6, 'Sibling of world famous parents who have disappeared', 0),
	(27, 'The Witch 2','Film', 7, 'Family forces of witchcraft, black magic and possession', 0),
	(28, 'Space station 76 2 ','Film', 5, 'A 1970s version of the future', 0),
    (29, 'Journey to the Center of the Earth 2','Film', 5, 'Discovering a fantastic the lost world in the centre of the earth', 0),
    (30, 'Bully 2','Film', 5, 'A documentary on peer-to-peer bullying in schools across America', 0);


	
	/* get more books from https://www.booktopia.com.au/the-outsider-stephen-king/prod9781473676404.html */

	
-- Content for table 'booktable'

	INSERT INTO `booktable` (`ItemID`, `Author`, `Publisher`, `Publishdate`, `ISBN`) VALUES

	(1, 'Yotam Ottolenghi', 'Ebury Publishing', '2018-09-06', '178503116'),
	(2, 'Scott Pape', 'HarperCollins', '2018-09-24', '146075687'),
	(3, 'Jane Harper', 'Pan Macmillan Australia ', '2018-10-23', '174354910'),
	(4, 'Stephen King', 'Hodder and Stoughton GD', '2018-05-22', '147367640'),
	(5, 'Martel Yann', 'Knopf Canada', '2001-09-11', '0676973760'),
	(6, 'Bill Clinton & James Patterson', 'Alfred Knopf and Little Brown & Co.', '2018-06-04', '978016412698');

-- Content for table 'cdtable'

	INSERT INTO `cdtable` (`ItemID`, `Artist`, `ReleaseDate`) VALUES

	(7, 'The Wiggles','2018-04-04'),
	(8, 'Kevin Wilson Bloody','2018-09-28'),
	(9, 'Battlebird','2016-03-25'),
	(10, 'Simple Minds','2018-02-02'),
	(11, 'John Krane','2012-07-30'),
	(12, 'Various Artists','2017-10-13'),
	(19, 'Various Artists','2017-10-13'),
	(20, 'Various Artists','2017-10-13'),
	(21, 'Various Artists','2017-10-13'),
	(22, 'Various Artists','2017-10-13'),
	(23, 'Various Artists','2017-10-13'),
	(24, 'Various Artists','2017-10-13');
	
	
	

	-- Content for table 'filmtable'

	INSERT INTO `filmtable` (`ItemID`, `Director`, `ReleaseDate`) VALUES
	(13, 'Danny Boyle','2011-02-22'),
	(14, 'Jason Bateman','2015-09-15'),
	(15, 'Robert Eggers','2015-01-27'),
	(16, 'Jack Plotnick','2014-09-19'),
    (17, 'Eric Brevig','2008-01-11'),
    (18, 'Lee Hirsch','2012-06-20'),
	(25, 'Multiple Directors','2014-03-21'),
	(26, 'Multiple Directors','2015-05-01'),
	(27, 'Multiple Directors','2016-03-19'),
	(28, 'Multiple Directors','2016-01-11'),
	(29, 'Multiple Directors','2017-03-22'),
	(30, 'Multiple Directors','2018-02-24');

		
-- Content for table 'loantable'

	INSERT INTO `loantable` (`LoanID`, `ItemID`, `UserID`,`ReturnDate`) VALUES
	(1, 2, 1,'2018-11-22'),
	(2, 1, 2,'2018-11-22'),
	(3, 4, 2,'2018-11-22'),
    (4, 6, 1,'2018-11-22'),
	(5, 9, 2,'2018-11-22'),
	(6, 11, 1,'2018-11-22'),
    (7, 13, 1,'2018-11-22'),
	(8, 17, 2,'2018-11-22');


-- Content for table 'commenttable'

	INSERT INTO `commenttable` (`CmntID`, `ItemID`, `UserID`, `Content`) VALUES
	(1, 2, 1, 'So good'),
	(2, 3, 2, 'Tough read.'),
	(3, 5, 1, 'Interesting'),
	(4, 6, 2, 'The storyline catches the readers attention'),
	(5, 15, 1, 'Good fiction story'),
	(6, 18, 2, 'Must watch');


-- Content for table 'requesttable'

	INSERT INTO `requesttable` (`UserID`, `ItemID`) VALUES
	(1, 3),
	(2, 2);

-- --------------------------------------------------------

-- Table Constraints ---------------------------------------


-- Constraints for table `booktable`
--
	ALTER TABLE `booktable`
	  ADD CONSTRAINT `booktable_ibfk_1` FOREIGN KEY (`ItemID`) REFERENCES `itemtable` (`ItemID`);

	  
-- Constraints for table `booktable`
--
	ALTER TABLE `cdtable`
	  ADD CONSTRAINT `cdtable_ibfk_1` FOREIGN KEY (`ItemID`) REFERENCES `itemtable` (`ItemID`);

	  
-- Constraints for table `booktable`
--
	ALTER TABLE `filmtable`
	  ADD CONSTRAINT `filmtable_ibfk_1` FOREIGN KEY (`ItemID`) REFERENCES `itemtable` (`ItemID`);

	  
	  -- Constraints for table `requesttable`
--
	ALTER TABLE `requesttable`
	  -- ADD CONSTRAINT `requesttable_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `usertable` (`UserID`),
	  ADD CONSTRAINT `requesttable_ibfk_2` FOREIGN KEY (`ItemID`) REFERENCES `itemtable` (`ItemID`);

--
-- Constraints for table `loantable`
--
	ALTER TABLE `loantable`
	  ADD CONSTRAINT `loantable_ibfk_1` FOREIGN KEY (`ItemID`) REFERENCES `itemtable` (`ItemID`),
	  ADD CONSTRAINT `loantable_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `usertable` (`UserID`);

--
-- Constraints for table `commenttable`
--
	ALTER TABLE `commenttable`
	  ADD CONSTRAINT `commenttable_ibfk_1` FOREIGN KEY (`ItemID`) REFERENCES `itemtable` (`ItemID`),
	  ADD CONSTRAINT `commenttable_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `usertable` (`UserID`);

	  
-- --------------------------------------------------------

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
